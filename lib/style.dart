import 'package:flutter/material.dart';

//Color
class MyColor {
  static int _convert(int value) {
    switch (value) {
      case 80:
        return 0xcc000000;
      case 50:
        return 0x80000000;
      case 20:
        return 0x33000000;
      default:
        return 0xff000000;
    }
  }

  static Color color1(int opacity) => Color(0x000039a9 + _convert(opacity));
  static Color color2(int opacity) => Color(0x00027dff + _convert(opacity));
  static Color color3(int opacity) => Color(0x003399ff + _convert(opacity));
  static Color color4(int opacity) => Color(0x0088d1f1 + _convert(opacity));
  static Color color5(int opacity) => Color(0x0041aade + _convert(opacity));
  static Color color6(int opacity) => Color(0x001392d3 + _convert(opacity));
  static Color white(int opacity) => Color(0x00ffffff + _convert(opacity));
  static Color grey(int opacity) => Color(0x00c4c4c4 + _convert(opacity));
  static Color color7(int opacity) => Color(0x008F8F8F + _convert(opacity));
  static Color red(int opacity) => Color(0x00DE4141 + _convert(opacity));
  static Color color8(int opacity) => Color(0x00636363 + _convert(opacity));
}

//Font Size
const double regularSize = 14;
const double largeSize = 36;

//FontFamily
const String fontLogo = 'Ruluko';
