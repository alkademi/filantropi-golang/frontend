// import 'dart:html';
// ignore_for_file: non_constant_identifier_names, unused_field

import 'dart:async';

import 'package:tes/screens/homepage/app_banner.dart';
import 'package:tes/screens/homepage/recommended_fundraising.dart';

import 'custom_sliver_delegate.dart';
import 'package:flutter/material.dart';
import 'package:tes/app.dart';
import 'dart:convert' show json, base64, ascii;
import '../../utils/secure_storage.dart';

class HomePage extends StatefulWidget {
  final String _jwt;
  final Map<String, dynamic> _payload;

  const HomePage(this._jwt, this._payload, {Key? key}) : super(key: key);

  factory HomePage.fromBase64(String jwt) => HomePage(
      jwt,
      json.decode(
          ascii.decode(base64.decode(base64.normalize(jwt.split(".")[1])))));

  @override
  State<HomePage> createState() => _HomePageStateWidget();
}

class _HomePageStateWidget extends State<HomePage> {
  final bool _pinned = true;
  final bool _floating = true;

  @override
  Widget build(BuildContext context) => Scaffold(
        body: SafeArea(
          child: Container(
            // height: MediaQuery.of(context).size.height * 1/2,
            color: Colors.white,
            child: CustomScrollView(
              slivers: <Widget>[
                SliverPersistentHeader(
                  pinned: _pinned,
                  floating: _floating,
                  delegate: CustomSliverDelegate(
                    expandedHeight: 108,
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate([
                    _MainFeature(context),
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 12,
                      child: const DecoratedBox(
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(196, 196, 196, 0.2))),
                    ),
                    _MainContainer(context, RecommendedFundraising(context)),
                    _MainContainer(
                        context,
                        SizedBox(
                          height: 158,
                          child: AdsBanner(),
                        )),
                    _seeRiwayatButton(context),
                    _logOutButton(context)
                  ]),
                )
              ],
            ),
          ),
        ),
      );

  Widget _MainFeature(BuildContext context) {
    Widget _IconButton(
        BuildContext context, String imgScr, String featureName, String func) {
      return Column(
        children: [
          CircleAvatar(
            backgroundColor: const Color.fromRGBO(65, 170, 222, 1),
            radius: 55 / 2,
            child: IconButton(
              icon: Image.asset(
                imgScr,
                color: Colors.white,
              ),
              onPressed: () => Navigator.pushNamed(context, func,
                  arguments: <String, dynamic>{}),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8),
            child: Text(featureName),
          )
        ],
      );
    }

    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 0),
      padding: const EdgeInsets.fromLTRB(20, 10, 20, 20),
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              _IconButton(
                  context, "assets/donation-icon.png", "Donasi", donationRoute),
              _IconButton(context, "assets/fundraising-icon.png", "Galang Dana",
                  createFundrasing),
            ],
          ),
        ],
      ),
    );
  }

  Widget _MainContainer(BuildContext context, Widget child) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          color: Colors.white,
          child: child,
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 12,
          child: const DecoratedBox(
            decoration:
                BoxDecoration(color: Color.fromRGBO(196, 196, 196, 0.2)),
          ),
        )
      ],
    );
  }

  Widget _logOutButton(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      height: 40,
      width: double.infinity,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
      child: ElevatedButton(
          onPressed: () => _onLogOut(context),
          child: const Text(
            'Log Out',
          )),
    );
  }

  Widget _seeRiwayatButton(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      height: 40,
      width: double.infinity,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
      child: ElevatedButton(
          onPressed: () => Navigator.pushNamed(context, donationHistory,
              arguments: <String, dynamic>{}),
          child: const Text(
            'Riwayat Donasi',
          )),
    );
  }

  Future<void> _onLogOut(BuildContext context) async {
    SecureStorage.deleteSecureData("jwt");
    Navigator.pushNamedAndRemoveUntil(context, loginRoute, (route) => false,
        arguments: <String, dynamic>{});
  }
}
