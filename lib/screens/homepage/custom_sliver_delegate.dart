import 'package:flutter/material.dart';
import 'package:tes/app.dart';

class CustomSliverDelegate extends SliverPersistentHeaderDelegate {
  final double expandedHeight;
  final bool hideTitleWhenExpanded;
  final cardsize = 87;

  CustomSliverDelegate({
    required this.expandedHeight,
    this.hideTitleWhenExpanded = true,
  });

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    final appBarSize = expandedHeight - shrinkOffset;
    final cardTopPosition = expandedHeight * 3 / 4 - shrinkOffset;
    final proportion = 2 - (expandedHeight / appBarSize);
    final percent = proportion < 0 || proportion > 1 ? 0.0 : proportion;

    return SizedBox(
      height: (expandedHeight + cardsize - (expandedHeight * 1 / 4)),
      child: Stack(
        children: <Widget>[
          SizedBox(
            height: appBarSize < kToolbarHeight ? kToolbarHeight : appBarSize,
            child: const CustomAppBar(),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            top: cardTopPosition > 0 ? cardTopPosition : 0,
            child: Opacity(
              opacity: percent,
              child: _WelcomeCard(percent: percent),
            ),
          )
        ],
      ),
    );
  }

  @override
  double get maxExtent => expandedHeight + cardsize - (expandedHeight * 1 / 4);

  @override
  double get minExtent => kToolbarHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}

class _WelcomeCard extends StatelessWidget {
  const _WelcomeCard({
    Key? key,
    required this.percent,
  }) : super(key: key);

  final double percent;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16 * percent),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        elevation: 5,
        child: Container(
          padding: const EdgeInsets.only(left: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Text(
                'Hai,',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(65, 170, 222, 1),
                    fontSize: 24),
              ),
              Text(
                'Orang Baik!',
                style: TextStyle(fontSize: 18),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CustomAppBar extends StatelessWidget {
  const CustomAppBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: const Color.fromRGBO(65, 170, 222, 1),
      elevation: 0,
      title: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            // Profile Picture
            Container(
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.black),
                  shape: BoxShape.circle,
                  image: const DecorationImage(
                      image: NetworkImage(
                          "https://static.wikia.nocookie.net/fategrandorder/images/a/a7/Gudao_Command_Seal.png/revision/latest/scale-to-width-down/400?cb=20190407035219"),
                      fit: BoxFit.cover)),
            ),
            // This is Search Bar
            Container(
              height: 40,
              width: 282,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: const _SearchBar(),
            )
          ],
        ),
      ),
    );
  }
}

class _SearchBar extends StatelessWidget {
  const _SearchBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, searchRoute,
          arguments: <String, dynamic>{}),
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: const [
            Padding(padding: EdgeInsets.only(left: 1.0)),
            Center(
                child: Text(
              'Coba cari "Tolong Menolong"',
              style: TextStyle(
                  fontSize: 13, color: Color.fromRGBO(196, 196, 196, 1)),
            )),
            Center(child: Icon(Icons.search, color: Colors.black, size: 20)),
            Padding(padding: EdgeInsets.only(right: 1.0))
          ]),
    );
  }
}
