// ignore_for_file: must_be_immutable, non_constant_identifier_names, avoid_types_as_parameter_names

import 'dart:async';

import 'package:flutter/material.dart';

class AppBanner {
  final int id;
  final String thumbnailUrl;

  AppBanner(this.id, this.thumbnailUrl);
}

//Sample Data
List<AppBanner> appBannerList = [
  AppBanner(1,
      "https://pedulibareng.com/assets/upload/galang/Donasi_Rutin_biaya_Pengobatan.jpg"),
  AppBanner(2,
      "https://70867a2ef4c36f4d1885-185a360f54556c7e8b9c7a9b6e422c6e.ssl.cf6.rackcdn.com/file/2021-08-10/3O6W2Rc9pYFC.jpg"),
  AppBanner(3,
      "https://img.kitabisa.cc/size/664x357/35944a08-a0b8-4148-9eca-485d7ed66481.jpg")
];

class AdsBanner extends StatefulWidget {
  List<AppBanner> appBannerList = [
    AppBanner(1,
        "https://pedulibareng.com/assets/upload/galang/Donasi_Rutin_biaya_Pengobatan.jpg"),
    AppBanner(2,
        "https://70867a2ef4c36f4d1885-185a360f54556c7e8b9c7a9b6e422c6e.ssl.cf6.rackcdn.com/file/2021-08-10/3O6W2Rc9pYFC.jpg"),
    AppBanner(3,
        "https://img.kitabisa.cc/size/664x357/35944a08-a0b8-4148-9eca-485d7ed66481.jpg")
  ];

  AdsBanner({Key? key}) : super(key: key);

  @override
  State<AdsBanner> createState() => _AdsBannerStateWidget();
}

class _AdsBannerStateWidget extends State<AdsBanner> {
  var _selectedIndexAdsBanner = 0;
  late Timer _timer;
  final PageController _pageControllerAds = PageController(initialPage: 0);

  void activateTimer() {
    _timer = Timer.periodic(const Duration(seconds: 4), (timer) {
      if (_selectedIndexAdsBanner < widget.appBannerList.length - 1) {
        _selectedIndexAdsBanner++;
      } else {
        _selectedIndexAdsBanner = 0;
      }

      _pageControllerAds.animateToPage(
        _selectedIndexAdsBanner,
        duration: const Duration(milliseconds: 350),
        curve: Curves.easeIn,
      );
    });
  }

  void deactivateTimer() {
    _timer.cancel();
  }

  @override
  void initState() {
    super.initState();
    activateTimer();
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _AdsBannerSub(context),
        Container(
          padding: const EdgeInsets.only(bottom: 5),
          alignment: Alignment.bottomCenter,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ...List.generate(
                  appBannerList.length,
                  (index) => IndicatorSub(
                        isActive:
                            _selectedIndexAdsBanner == index ? true : false,
                      ))
            ],
          ),
        )
      ],
    );
  }

  Widget _AdsBannerSub(BuildContext context) {
    Widget _AdsBannerContainerSub(BuildContext context, String src) {
      return Container(
        height: 118,
        width: 330,
        decoration: BoxDecoration(
            image:
                DecorationImage(image: NetworkImage(src), fit: BoxFit.cover)),
      );
    }

    return GestureDetector(
      onTapDown: (TapDownDetails) => deactivateTimer(),
      onTapUp: (TapUpDetails) => activateTimer(),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(5),
        child: PageView.builder(
          onPageChanged: (index) {
            setState(() {
              _selectedIndexAdsBanner = index;
            });
          },
          controller: _pageControllerAds,
          itemCount: appBannerList.length,
          itemBuilder: (context, index) {
            return _AdsBannerContainerSub(
                context, widget.appBannerList[index].thumbnailUrl);
          },
        ),
      ),
    );
  }
}

class IndicatorSub extends StatelessWidget {
  final bool isActive;
  const IndicatorSub({Key? key, this.isActive = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 350),
      margin: const EdgeInsets.symmetric(horizontal: 4),
      width: isActive ? 44 / 3 : 8.0,
      height: 8.0,
      decoration: BoxDecoration(
          color:
              isActive ? const Color.fromRGBO(65, 170, 222, 1) : Colors.white,
          borderRadius: BorderRadius.circular(10)),
    );
  }
}
