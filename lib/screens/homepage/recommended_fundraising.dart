// ignore_for_file: non_constant_identifier_names, must_be_immutable

import 'package:flutter/material.dart';

Widget RecommendedFundraising(BuildContext context) {
  return Column(
    children: <Widget>[
      Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.only(left: 5, bottom: 10),
        child: const Text(
          'Penggalangan Danna',
          style: TextStyle(color: Color.fromRGBO(143, 143, 143, 1)),
        ),
      ),
      SizedBox(
        height: 112,
        width: 350,
        child: ListView.separated(
            scrollDirection: Axis.horizontal,
            itemBuilder: (BuildContext context, int index) => Container(
                  padding: const EdgeInsets.fromLTRB(5, 0, 5, 9),
                  // Data Dummy
                  child: _DonationEntry(
                    src:
                        'https://yatimmandiri.org/uploads/slider/781d0234a39d1b0e1a1db4251ccdc832.jpg',
                    title: 'Corona Relief Support for Indonesia',
                    progressValue: 0.3,
                    targetValue: 'Rp1.250.000',
                    remainingTime: '19 hari lagi',
                  ),
                ),
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(),
            itemCount: 3),
      )
    ],
  );
}

class _DonationEntry extends StatelessWidget {
  double width = 162;
  double height = 98;

  final String src;
  final String title;
  final double progressValue;
  final String targetValue;
  final String remainingTime;

  _DonationEntry(
      {Key? key,
      required this.src,
      required this.title,
      required this.progressValue,
      required this.targetValue,
      required this.remainingTime})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.25),
              spreadRadius: 0,
              blurRadius: 4,
              offset: Offset(0, 5),
            )
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(5), topLeft: Radius.circular(5)),
                image: DecorationImage(
                    fit: BoxFit.cover, image: NetworkImage(src))),
            height: height * 4 / 6,
            width: width,
          ),
          Flexible(
              child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 4),
            child: Text(
              title,
              style: const TextStyle(fontSize: 6, fontWeight: FontWeight.bold),
            ),
          )),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 6),
            margin: const EdgeInsets.symmetric(vertical: 2),
            height: 3,
            child: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              child: LinearProgressIndicator(
                color: const Color.fromRGBO(136, 209, 241, 1),
                backgroundColor: const Color.fromRGBO(196, 196, 196, 0.8),
                value: progressValue,
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 6),
            margin: const EdgeInsets.symmetric(vertical: 2),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  targetValue,
                  style:
                      const TextStyle(fontSize: 6, fontWeight: FontWeight.bold),
                ),
                Text(remainingTime,
                    style: TextStyle(
                        fontSize: 5, color: Colors.grey.withOpacity(0.7)))
              ],
            ),
          )
        ],
      ),
    );
  }
}
