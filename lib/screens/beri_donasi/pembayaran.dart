import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PayDonation extends StatelessWidget {
  final String url;
  final currencyFormatter = NumberFormat('#,##0', 'ID');
  PayDonation(this.url, {Key? key}) : super(key: key);

  @override
  // WebViewExampleState createState() => WebViewExampleState();
  Widget build(BuildContext context) {
    return SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: WebView(
          initialUrl: url,
          javascriptMode: JavascriptMode.unrestricted,
        ));
  }
}

// class WebViewExampleState extends State<PayDonation> {
//   @override
//   void initState() {
//     super.initState();
//     // Enable virtual display.
//     if (Platform.isAndroid) WebView.platform = AndroidWebView();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return const WebView(
//       initialUrl: 'pajak.io',
//     );
//   }
// }
