import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tes/utils/secure_storage.dart';
import '../../app.dart';
import '../../constants/server_ip.dart';
import 'package:http/http.dart' as http;
import '../../style.dart';

class BeriDonasi extends StatelessWidget {
  final int donationID;
  final currencyFormatter = NumberFormat('#,##0', 'ID');
  BeriDonasi(this.donationID, {Key? key}) : super(key: key);
  final TextEditingController _nominalController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getDonationData,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return const Center(
                  child: SizedBox(
                child: CircularProgressIndicator(),
                width: 50,
                height: 50,
              ));
            default:
              if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              } else {
                if (snapshot.hasData) {
                  return Scaffold(
                    body: Column(
                      children: <Widget>[
                        SizedBox(
                          width: double.infinity,
                          child: FittedBox(
                            fit: BoxFit.fitWidth,
                            child: Image.asset('assets/donation_default.jpg'),
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(bottom: 10.0)),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              const Padding(
                                padding: EdgeInsets.only(
                                  left: 15,
                                ), //apply padding to some sides only
                                child: Text(
                                  "Berapa Banyak Ingin Kamu Donasikan ?",
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                      fontFamily: 'Open Sans',
                                      color: Colors.black),
                                ),
                              ),
                              IconButton(
                                icon: const Icon(Icons.close,
                                    color: Colors.black, size: 20),
                                highlightColor: Colors.blue,
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                            ]),
                        _optionNominal(),
                        _donateButton(context)
                      ],
                    ),
                  );
                } else {
                  return Scaffold(
                      appBar: AppBar(
                        title: const Text('Detail Donasi',
                            style: TextStyle(
                                fontSize: 20, fontFamily: 'Open Sans')),
                        backgroundColor: MyColor.color5(100),
                      ),
                      body: const Center(
                        child: Text("Donation Detail Not Found"),
                      ));
                }
              }
          }
        });
  }

  Widget _donateButton(context) {
    return Container(
      transform: Matrix4.translationValues(0.0, -20.0, 0.0),
      margin: const EdgeInsets.only(bottom: 20, left: 15, right: 15),
      height: 45,
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            spreadRadius: 0,
            blurRadius: 4,
            offset: const Offset(0, 2),
          ),
        ],
      ),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(primary: MyColor.color5(100)),
          onPressed: () {
            var nominal = int.tryParse(_nominalController.text) ?? 0;
            giveDonation(nominal, false, context);
          },
          child: Text(
            'Bayar Sekarang',
            style: TextStyle(
                color: MyColor.white(100),
                fontSize: 18,
                fontWeight: FontWeight.w900),
          )),
    );
  }

  Widget _optionNominal() {
    return Container(
        transform: Matrix4.translationValues(0.0, -30.0, 0.0),
        child: ListView(
          shrinkWrap: true,
          children: [
            GestureDetector(
              onTap: () {
                _nominalController.text = "1000";
              },
              child: Container(
                  margin: const EdgeInsets.only(left: 15, right: 15),
                  height: 60,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: MyColor.white(100),
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: MyColor.grey(100)),
                  ),
                  child: const Center(
                    child: Text(
                      'Rp 1.000',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w400),
                    ),
                  )),
            ),
            const Padding(padding: EdgeInsets.only(bottom: 10)),
            GestureDetector(
              onTap: () {
                _nominalController.text = "10000";
              },
              child: Container(
                  margin: const EdgeInsets.only(left: 15, right: 15),
                  height: 60,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: MyColor.white(100),
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: MyColor.grey(100)),
                  ),
                  child: const Center(
                    child: Text(
                      'Rp 10.000',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w400),
                    ),
                  )),
            ),
            const Padding(padding: EdgeInsets.only(bottom: 10)),
            GestureDetector(
              onTap: () {
                _nominalController.text = "50000";
              },
              child: Container(
                  margin: const EdgeInsets.only(left: 15, right: 15),
                  height: 60,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: MyColor.white(100),
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: MyColor.grey(100)),
                  ),
                  child: const Center(
                    child: Text(
                      'Rp 50.000',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w400),
                    ),
                  )),
            ),
            const Padding(padding: EdgeInsets.only(bottom: 10)),
            GestureDetector(
              onTap: () {
                _nominalController.text = "100000";
              },
              child: Container(
                  margin: const EdgeInsets.only(left: 15, right: 15),
                  height: 60,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: MyColor.white(100),
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: MyColor.grey(100)),
                  ),
                  child: const Center(
                    child: Text(
                      'Rp 100.000',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w400),
                    ),
                  )),
            ),
            const Padding(padding: EdgeInsets.only(bottom: 10)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text(
                  'Atau',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.w800),
                )
              ],
            ),
            _amountInput(),
          ],
        ));
  }

  Widget _amountInput() {
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: MyColor.color1(20),
        ),
        margin: const EdgeInsets.only(top: 7, bottom: 10, left: 15, right: 15),
        height: 60,
        width: double.infinity,
        child: Row(
          children: [
            Container(
              margin: const EdgeInsets.only(
                left: 15,
                right: 15,
              ),
              child: const Text(
                'Rp',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w800),
              ),
            ),
            Expanded(
                child: TextFormField(
              keyboardType: TextInputType.number,
              controller: _nominalController,
              cursorColor: MyColor.white(100),
              style: TextStyle(
                  color: MyColor.white(100),
                  fontSize: 18,
                  fontWeight: FontWeight.w800),
              decoration: InputDecoration(
                  // filled: true,
                  // fillColor: MyColor.color1(20),
                  errorStyle: const TextStyle(fontSize: regularSize),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: BorderSide(width: 0, color: MyColor.color1(20)),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide:
                          BorderSide(width: 0, color: MyColor.color1(20))),
                  contentPadding:
                      const EdgeInsets.only(left: 10, top: 11, bottom: 10),
                  hintText: '0',
                  hintStyle: TextStyle(
                      color: MyColor.white(100),
                      fontSize: 18,
                      fontWeight: FontWeight.w800)),
            ))
          ],
        ));
  }

  Future<Map<String, dynamic>?> get getDonationData async {
    var url = Uri.parse('$serverIP/api/v1/fundraisings/$donationID');
    var res = await http.get(url);
    if (res.statusCode == 200) return jsonDecode(res.body)["data"];
    return null;
  }

  Future<void> giveDonation(
      int amount, bool isAnonymous, BuildContext context) async {
    var url = Uri.parse('$serverIP/api/v1/fundraisings/$donationID/donate');
    var jwt = await SecureStorage.readSecureData("jwt");
    var res = await http.post(url,
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $jwt'
        },
        body: jsonEncode({"amount": amount, "is_anonymous": isAnonymous}));
    if (res.statusCode == 201) {
      debugPrint(jsonDecode(res.body)["data"]["midtrans_url"].toString());
      Navigator.pushNamed(context, payRoute, arguments: {
        'url': jsonDecode(res.body)["data"]["midtrans_url"].toString()
      });
    }
  }
}
