import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:tes/app.dart';
import '../../style.dart';
import 'package:http/http.dart' as http;
import '../../utils/secure_storage.dart';
import '../../constants/server_ip.dart';

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key? key}) : super(key: key);

  @override
  _RegisterForm createState() => _RegisterForm();
}

class _RegisterForm extends State<RegisterForm> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _invalid = false;

  @override
  void dispose() {
    _usernameController.dispose();
    _phoneController.dispose();
    _nameController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return Builder(
        builder: (context) => Form(
            key: _formKey,
            child: Container(
              height: _invalid ? 170 : 425,
              margin: const EdgeInsets.only(left: 15, right: 15),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    _nameInput(),
                    _usernameInput(),
                    _phoneInput(),
                    _passwordInput(),
                    _submitButton(),
                  ]),
            )));
  }

  Widget _usernameInput() {
    return TextFormField(
      controller: _usernameController,
      validator: (value) => _validator(value),
      cursorColor: MyColor.white(100),
      style: TextStyle(color: MyColor.white(100), fontSize: regularSize),
      decoration: InputDecoration(
          filled: true,
          fillColor: MyColor.color1(20),
          errorStyle: const TextStyle(fontSize: regularSize),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(width: 0, color: MyColor.color1(20))),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(width: 0, color: MyColor.color1(20))),
          contentPadding: const EdgeInsets.only(left: 10, top: 11, bottom: 10),
          hintText: 'Username',
          hintStyle:
              TextStyle(color: MyColor.white(100), fontSize: regularSize)),
    );
  }

  Widget _passwordInput() {
    return Column(
      children: [
        TextFormField(
          controller: _passwordController,
          obscureText: true,
          validator: (value) => _validator(value),
          cursorColor: MyColor.white(100),
          style: TextStyle(color: MyColor.white(100), fontSize: regularSize),
          decoration: InputDecoration(
              filled: true,
              fillColor: MyColor.color1(20),
              errorStyle: const TextStyle(fontSize: regularSize),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(width: 0, color: MyColor.color1(20)),
              ),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(width: 0, color: MyColor.color1(20))),
              contentPadding:
                  const EdgeInsets.only(left: 10, top: 11, bottom: 10),
              hintText: 'Kata Sandi',
              hintStyle:
                  TextStyle(color: MyColor.white(100), fontSize: regularSize)),
        ),
        const SizedBox(height: 20),
        TextFormField(
          controller: _confirmPasswordController,
          obscureText: true,
          validator: (value) => _confirmPasswordValidator(value),
          cursorColor: MyColor.white(100),
          style: TextStyle(color: MyColor.white(100), fontSize: regularSize),
          decoration: InputDecoration(
              filled: true,
              fillColor: MyColor.color1(20),
              errorStyle: const TextStyle(fontSize: regularSize),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(width: 0, color: MyColor.color1(20)),
              ),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(width: 0, color: MyColor.color1(20))),
              contentPadding:
                  const EdgeInsets.only(left: 10, top: 11, bottom: 10),
              hintText: 'Konfirmasi Kata Sandi',
              hintStyle:
                  TextStyle(color: MyColor.white(100), fontSize: regularSize)),
        )
      ],
    );
  }

  Widget _nameInput() {
    return TextFormField(
      controller: _nameController,
      validator: (value) => _validator(value),
      cursorColor: MyColor.white(100),
      style: TextStyle(color: MyColor.white(100), fontSize: regularSize),
      decoration: InputDecoration(
          filled: true,
          fillColor: MyColor.color1(20),
          errorStyle: const TextStyle(fontSize: regularSize),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(width: 0, color: MyColor.color1(20)),
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(width: 0, color: MyColor.color1(20))),
          contentPadding: const EdgeInsets.only(left: 10, top: 11, bottom: 10),
          hintText: 'Nama Lengkap',
          hintStyle:
              TextStyle(color: MyColor.white(100), fontSize: regularSize)),
    );
  }

  Widget _phoneInput() {
    return TextFormField(
      keyboardType: TextInputType.number,
      controller: _phoneController,
      validator: (value) => _validator(value),
      cursorColor: MyColor.white(100),
      style: TextStyle(color: MyColor.white(100), fontSize: regularSize),
      decoration: InputDecoration(
          filled: true,
          fillColor: MyColor.color1(20),
          errorStyle: const TextStyle(fontSize: regularSize),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(width: 0, color: MyColor.color1(20)),
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(width: 0, color: MyColor.color1(20))),
          contentPadding: const EdgeInsets.only(left: 10, top: 11, bottom: 10),
          hintText: 'Nomor Telepon',
          hintStyle:
              TextStyle(color: MyColor.white(100), fontSize: regularSize)),
    );
  }

  Widget _submitButton() {
    return Container(
      height: 40,
      width: double.infinity,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(primary: MyColor.color5(100)),
          onPressed: () => _onSubmit(),
          child: Text(
            'Daftar',
            style: TextStyle(color: MyColor.white(100), fontSize: regularSize),
          )),
    );
  }

  String? _validator(String? value) {
    if (value!.isEmpty) {
      _invalid = true;
      return 'Input Ini Harus Diisi';
    }
    _invalid = false;
    return null;
  }

  String? _confirmPasswordValidator(String? value) {
    if (value!.isEmpty) {
      _invalid = true;
      return 'Input Ini Harus Diisi';
    }
    if (value != _passwordController.text) {
      _invalid = true;
      return 'Password tidak sesuai';
    }
    _invalid = false;
    return null;
  }

  Future<void> _onSubmit() async {
    if (_formKey.currentState!.validate()) {
      var name = _nameController.text;
      var username = _usernameController.text;
      var password = _passwordController.text;
      var phone = _phoneController.text;
      var jwt = await attemptRegister(name, username, password, phone);
      if (jwt != null) {
        SecureStorage.writeSecureData("jwt", jwt);
        Navigator.pushReplacementNamed(context, homeRoute,
            arguments: {'jwt': jwt});
      } else {
        displayDialog(context, "Terjadi Error",
            "Tidak ada akun dengan username dan password yang sesuai");
      }
    }
  }

  Future<String?> attemptRegister(
      String name, String username, String password, String phone) async {
    var url = Uri.parse('$serverIP/api/v1/auth/register');
    var res = await http.post(url,
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: jsonEncode({
          "Username": username,
          "Password": password,
          "Name": name,
          "Phone_Number": phone
        }));
    if (res.statusCode == 201) return jsonDecode(res.body)["data"]["token"];
    return null;
  }

  void displayDialog(context, title, text) => showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(title: Text(title), content: Text(text)),
      );
}
