import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tes/app.dart';
import '../../constants/server_ip.dart';
import '../../style.dart';
import 'package:http/http.dart' as http;
import '../../utils/secure_storage.dart';

class Riwayat extends StatefulWidget {
  const Riwayat({Key? key}) : super(key: key);

  @override
  _RiwayatDonasi createState() => _RiwayatDonasi();
}

class _RiwayatDonasi extends State<Riwayat> {
  final _currencyFormatter = NumberFormat("#,##0", "ID");
  int _page = 0;
  final int _limit = 10;
  bool _hasNextPage = true;
  bool _isFirstLoadRunning = false;
  bool _isLoadMoreRunning = false;
  bool _isEmpty = false;
  final List _riwayat = [];

  void _firstLoad() async {
    setState(() {
      _isFirstLoadRunning = true;
    });
    try {
      var jwt = await SecureStorage.readSecureData("jwt");
      final res = await http.get(
        Uri.parse("$serverIP/api/v1/donations/my/?_page=$_page&_limit=$_limit"),
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $jwt'
        },
      );
      if (res.statusCode != 200) {
        throw Exception(res.statusCode);
      }
      final List fetchedPosts = json.decode(res.body)["data"];
      if (fetchedPosts.isNotEmpty) {
        setState(() {
          _riwayat.addAll(fetchedPosts);
        });
      } else {
        setState(() {
          _isEmpty = true;
        });
      }
    } catch (err) {
      if (kDebugMode) {
        print(err);
      }
    }

    setState(() {
      _isFirstLoadRunning = false;
    });
  }

  void _loadMore() async {
    if (_hasNextPage == true &&
        _isFirstLoadRunning == false &&
        _isLoadMoreRunning == false &&
        _controller.position.extentAfter < 300) {
      setState(() {
        _isLoadMoreRunning = true;
      });
      _page += 1;
      try {
        var jwt = await SecureStorage.readSecureData("jwt");
        final res = await http.get(
          Uri.parse(
              "$serverIP/api/v1/donations/my/?_page=$_page&_limit=$_limit"),
          headers: {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer $jwt'
          },
        );
        if (res.statusCode != 200) {
          throw Exception(res.statusCode);
        }
        final List fetchedPosts = json.decode(res.body)["data"];
        if (_page == json.decode(res.body)["total_pages"]) {
          setState(() {
            _riwayat.addAll(fetchedPosts);
          });
        } else {
          setState(() {
            _hasNextPage = false;
          });
        }
      } catch (err) {
        if (kDebugMode) {
          print(err);
        }
      }

      setState(() {
        _isLoadMoreRunning = false;
      });
    }
  }

  late ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _firstLoad();
    _controller = ScrollController()..addListener(_loadMore);
  }

  @override
  void dispose() {
    _controller.removeListener(_loadMore);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget thumbnail = Container(
      decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(10.0),
              bottomRight: Radius.circular(10.0)),
          color: Colors.pink),
    );
    return Scaffold(
        appBar: AppBar(
          title: const Text('Donasiku', style: TextStyle(fontSize: 20)),
          backgroundColor: MyColor.color5(100),
        ),
        body: _isFirstLoadRunning
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Expanded(
                      child: _isEmpty
                          ? Center(
                              child: Text(
                                "Kamu Belum Berdonasi",
                                style: TextStyle(
                                    color: MyColor.grey(100),
                                    fontWeight: FontWeight.w800),
                              ),
                            )
                          : ListView.builder(
                              controller: _controller,
                              itemCount: _riwayat.length,
                              itemBuilder: (_, index) => GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: () => Navigator.pushNamed(
                                    context, donationDetail, arguments: {
                                  'id': _riwayat[index]["fundraising"]["id"]
                                }),
                                child: _riwayatItemList(
                                    context,
                                    _riwayat[index]["amount"],
                                    _riwayat[index]["fundraising"]["title"],
                                    _riwayat[index]["status"],
                                    DateFormat("dd MMMM yyyy").format(
                                        DateTime.parse(
                                            _riwayat[index]["created_at"])),
                                    thumbnail),
                              ),
                            ),
                    ),
                    if (_isLoadMoreRunning == true)
                      const Padding(
                        padding: EdgeInsets.only(top: 10, bottom: 40),
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    if (_hasNextPage == false)
                      Container(
                        padding: const EdgeInsets.only(top: 10, bottom: 10),
                        color: MyColor.color1(100),
                        child: const Center(
                          child: Text(
                            'Semua Riwayat Donasimu Sudah Dimuat',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                  ],
                ),
              ));
  }

  Widget _riwayatDescription(
      int donated, String title, String status, String date) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 2,
              child: Text(
                title,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                    color: Colors.black),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            _statusContainer(status)
          ],
        ),
        const Padding(padding: EdgeInsets.symmetric(vertical: 10.0)),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "Rp${_currencyFormatter.format(donated)}',",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12,
                color: MyColor.color2(100),
              ),
            ),
            Text(
              date,
              style: TextStyle(
                fontWeight: FontWeight.w300,
                fontSize: 12,
                color: MyColor.color7(100),
              ),
            ),
          ],
        )
      ],
    );
  }

  Widget _riwayatItemList(BuildContext context, int donated, String title,
      String status, String date, Widget thumbnail) {
    return Wrap(children: [
      Column(children: [
        Padding(
            padding:
                const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 5),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: 100,
                  height: 90,
                  child: FittedBox(
                      fit: BoxFit.fill,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.asset('assets/donation_default.jpg'),
                      )),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 0.0, 2.0, 0.0),
                    child: _riwayatDescription(donated, title, status, date),
                  ),
                ),
              ],
            )),
        Divider(
          color: MyColor.grey(100),
          thickness: 3,
        )
      ]),
    ]);
  }

  Widget _statusContainer(String status) {
    Color col;
    if (status == "Berhasil") {
      col = MyColor.color5(100);
    } else if (status == "Batal") {
      col = MyColor.red(100);
    } else {
      col = MyColor.color8(100);
    }
    return Container(
        width: 70,
        height: 30,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(5)),
          border: Border.all(color: col),
        ),
        child: Center(
          child: Text(
            status,
            style: TextStyle(color: col),
          ),
        ));
  }
}
