import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import '../../constants/server_ip.dart';

class DaftarDonatur extends StatelessWidget {
  final int donationID;
  final currencyFormatter = NumberFormat('#,##0', 'ID');
  DaftarDonatur(this.donationID, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: <Widget>[
        SizedBox(
          width: double.infinity,
          child: FittedBox(
            fit: BoxFit.fitWidth,
            child: Image.asset('assets/donation_default.jpg'),
          ),
        ),
        const Padding(padding: EdgeInsets.only(bottom: 15.0)),
        Expanded(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
              const Padding(
                padding: EdgeInsets.only(
                    left: 15), //apply padding to some sides only
                child: Text(
                  "Daftar Donatur",
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontFamily: 'Open Sans',
                      color: Colors.black),
                ),
              ),
              IconButton(
                icon: const Icon(Icons.close, color: Colors.black, size: 20),
                highlightColor: Colors.blue,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ])),
        Expanded(
          flex: 20,
          child: FutureBuilder(
            future: getDonaturData(),
            builder: (context, snapshot) {
              if (snapshot.data == null) {
                return const Center(
                  child: Text('Loading ...'),
                );
              } else {
                var data = snapshot.data as List<Donation>;
                return ListView.builder(
                  shrinkWrap: true,
                  itemCount: data.length,
                  itemBuilder: (context, i) {
                    var name = "";
                    if (data[i].anon) {
                      name = "Anonimus";
                    } else {
                      name = data[i].name;
                    }
                    var amount = data[i].amount;
                    var donationTime = DateTime.parse(data[i].time);
                    var time = donationTime
                        .difference(DateTime.now())
                        .inDays
                        .toInt()
                        .abs();
                    return BaseListDonatur(
                      profilpic: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: const Color(0xFF3399FF)),
                      ),
                      name: name,
                      time: time,
                      amount: amount,
                    );
                  },
                );
              }
            },
          ),
        ),
      ],
    ));
  }

  Future<List<Donation>?> getDonaturData() async {
    var url = Uri.parse('$serverIP/api/v1/fundraisings/$donationID/donations');
    var res = await http.get(url);
    var jsonData = jsonDecode(res.body)["data"];

    List<Donation> donations = [];

    if (jsonData != null) {
      for (var u in jsonData) {
        if (u["status"] == "success") {
          Donation donation = Donation(u["user"]["name"], u["created_at"],
              u["amount"], u["is_anonymous"]);
          donations.add(donation);
        }
      }
    }
    return donations;
  }
}

class Donation {
  final String name, time;
  final int amount;
  final bool anon;

  Donation(this.name, this.time, this.amount, this.anon);
}

class _DonaturDescription extends StatelessWidget {
  const _DonaturDescription({
    Key? key,
    required this.name,
    required this.time,
    required this.amount,
  }) : super(key: key);

  final String name;
  final int time;
  final int amount;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 15),
        Row(children: <Widget>[
          Expanded(
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(left: 5.0),
                  child: Text(
                    name,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        fontFamily: 'Open Sans',
                        color: Colors.black),
                  ),
                ),
                const Padding(padding: EdgeInsets.only(bottom: 10.0)),
                Container(
                  margin: const EdgeInsets.only(left: 5.0),
                  child: Text("berdonasi $time hari lalu",
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        fontFamily: 'Open Sans',
                        fontWeight: FontWeight.normal,
                        fontSize: 10.0,
                        color: Colors.black,
                      )),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: const <Widget>[],
              ),
              const Padding(padding: EdgeInsets.only(bottom: 5.0)),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                        padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
                        child: Text(
                          "+ Rp $amount",
                          style: const TextStyle(
                            fontFamily: 'Open Sans',
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0,
                            color: Color(0xDD1392D3),
                          ),
                        )),
                  ),
                ],
              )
            ]),
          ),
        ]),
      ],
    );
  }
}

class BaseListDonatur extends StatelessWidget {
  const BaseListDonatur({
    Key? key,
    required this.profilpic,
    required this.name,
    required this.time,
    required this.amount,
  }) : super(key: key);

  final String name;
  final int time;
  final int amount;
  final Widget profilpic;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: const Color.fromRGBO(0, 57, 169, 0.1),
          borderRadius: BorderRadius.circular(10),
          // ignore: prefer_const_literals_to_create_immutables
          boxShadow: [
            const BoxShadow(
              color: Color.fromRGBO(0, 57, 169, 0.1),
              spreadRadius: 3,
              blurRadius: 7,
              offset: Offset(0.0, 0.75), // changes position of shadow
            ),
          ],
        ),

        // color: const Color(0xFF0039A9).withOpacity(0.1),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: SizedBox(
            height: 70,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                AspectRatio(
                  aspectRatio: 1,
                  child: profilpic,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 0.0, 2.0, 0.0),
                    child: _DonaturDescription(
                      name: name,
                      time: time,
                      amount: amount,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
