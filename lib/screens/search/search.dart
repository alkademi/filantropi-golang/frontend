import 'package:flutter/material.dart';
import 'package:tes/app.dart';
import 'package:http/http.dart' as http;
import '../../constants/server_ip.dart';
import 'dart:convert';

class Search extends StatefulWidget {
  const Search({Key? key}) : super(key: key);

  @override
  _Search createState() => _Search();
}

class _Search extends State<Search> {
  final TextEditingController _searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: <Widget>[
        const Padding(padding: EdgeInsets.only(bottom: 60.0)),
        Expanded(
          child: TextField(
            controller: _searchController,
            decoration: InputDecoration(
                prefixIcon: IconButton(
                  icon: const Icon(Icons.arrow_back,
                      color: Colors.black, size: 20),
                  highlightColor: Colors.blue,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                suffixIcon: IconButton(
                  icon: const Icon(Icons.close, color: Colors.black, size: 20),
                  highlightColor: Colors.blue,
                  onPressed: () {
                    _searchController.text = "";
                  },
                ),
                hintText: 'Coba cari "Tolong Menolong"',
                hintStyle: const TextStyle(
                    fontSize: 15, color: Color.fromRGBO(196, 196, 196, 1)),
                border: const UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey),
                ),
                contentPadding: const EdgeInsets.fromLTRB(15, 13, 0, 15)),
          ),
        ),
        SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height - 110,
            child: Fundraising(
              search: _searchController.text.toString(),
            )),
      ],
    ));
  }
}

class Fundraising extends StatefulWidget {
  final String search;
  const Fundraising({Key? key, required this.search}) : super(key: key);

  @override
  _FundraisingState createState() => _FundraisingState();
}

class _FundraisingState extends State<Fundraising> {
  getFundraisingData() async {
    var search = widget.search;
    var url = Uri.parse('$serverIP/api/v1/fundraisings?search=title:$search');
    debugPrint(url.toString());
    var res = await http.get(url);
    var jsonData = jsonDecode(res.body)["data"];

    List<Fund> funds = [];

    if (jsonData != null) {
      for (var u in jsonData) {
        Fund fund = Fund(u["id"], u["title"], u["receiver"]);
        funds.add(fund);
      }
    }

    return funds;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Card(
      child: FutureBuilder(
        future: getFundraisingData(),
        builder: (context, snapshot) {
          if (snapshot.data == null) {
            return const Center(
              child: Text('Loading ...'),
            );
          } else {
            var data = snapshot.data as List<Fund>;
            return ListView.builder(
              shrinkWrap: true,
              itemCount: data.length,
              itemBuilder: (context, i) {
                var id = data[i].id;
                var title = data[i].title;
                var receiver = data[i].receiver;
                return Row(
                  children: [
                    Expanded(
                        child: BaseListDonation(
                      id: id,
                      title: title,
                      author: receiver,
                      thumbnail: Container(
                        decoration: BoxDecoration(
                          image: const DecorationImage(
                            image: AssetImage('assets/donation_default.jpg'),
                            fit: BoxFit.fill,
                          ),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ))
                  ],
                );
              },
            );
          }
        },
      ),
    ));
  }
}

class Fund {
  final int id;
  final String title, receiver;

  Fund(this.id, this.title, this.receiver);
}

class _DonationDescription extends StatelessWidget {
  const _DonationDescription({
    Key? key,
    required this.title,
    required this.author,
  }) : super(key: key);

  final String author;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 2,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(left: 5.0, top: 15),
                child: Text(
                  title,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      fontFamily: 'Open Sans',
                      color: Colors.black),
                ),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 10.0)),
              Container(
                margin: const EdgeInsets.only(left: 5.0),
                child: Text(author,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      fontFamily: 'Open Sans',
                      fontWeight: FontWeight.normal,
                      fontSize: 14.0,
                      color: Colors.grey,
                    )),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class BaseListDonation extends StatelessWidget {
  const BaseListDonation({
    Key? key,
    required this.thumbnail,
    required this.title,
    required this.author,
    required this.id,
  }) : super(key: key);

  final String author;
  final Widget thumbnail;
  final String title;
  final int id;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () =>
            Navigator.pushNamed(context, donationDetail, arguments: {'id': id}),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: SizedBox(
            height: 90,
            child: Container(
                color: Colors.white,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    AspectRatio(
                      aspectRatio: 1.5,
                      child: thumbnail,
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10.0, 0.0, 2.0, 0.0),
                        child: _DonationDescription(
                          title: title,
                          author: author,
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ));
  }
}
