import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:tes/app.dart';
import '../../constants/server_ip.dart';
import 'package:http/http.dart' as http;
import '../../style.dart';
import 'package:expand_widget/expand_widget.dart';
import 'package:tuple/tuple.dart';

import '../../utils/secure_storage.dart';

class DonationDetail extends StatefulWidget {
  final int donationID;

  DonationDetail(this.donationID, {Key? key}) : super(key: key);

  @override
  State<DonationDetail> createState() => _DonationDetailState();
}

class _DonationDetailState extends State<DonationDetail> {
  final currencyFormatter = NumberFormat('#,##0', 'ID');
  final TextEditingController _closeDescription = TextEditingController();
  var isActive;

  List<String> list_kategori = <String>[
    "Kategori 1",
    "Kategori 2",
    "Kategori 3",
    "Kategori 4",
    "Kategori 5",
    "Kategori 6",
    "Kategori 7",
    "Kategori 8"
  ];
  var selectedCategory;
  String isLoading = 'Kirim';

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Future.wait([_getDonationData, _getDonorsData]),
        builder: (context, AsyncSnapshot<List> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return const Center(
                  child: SizedBox(
                child: CircularProgressIndicator(),
                width: 50,
                height: 50,
              ));
            default:
              if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              } else {
                if (snapshot.hasData) {
                  var donationData = snapshot.data![0];
                  var donorsData = snapshot.data![1];
                  var targetDate = DateTime.parse(
                      donationData["target_date"] ?? DateTime.now());
                  var donationPercentage = donationData["current_donation"] /
                          donationData["target_donation"] ??
                      0;
                  if (donationPercentage > 1) {
                    donationPercentage = 1.0;
                  }
                  var title = donationData['title'] ?? "Tidak ada Judul";
                  var currentDonation = donationData['current_donation'] ?? 0;
                  var targetDonation = donationData['target_donation'] ?? 0;
                  var countDonor = donationData['count_donor'] ?? 0;
                  var receiver = donationData['receiver'] ?? 'Tidak ditemukan';
                  var desc = donationData['description'] ?? 'Tidak ditemukan';
                  isActive = donationData['is_active'] ?? false;
                  var remainingDaysInt =
                      targetDate.difference(DateTime.now()).inDays;
                  var remainingDays =
                      remainingDaysInt > 0 ? remainingDaysInt.toString() : "0";
                  return Scaffold(
                    body: Column(
                      children: <Widget>[
                        SizedBox(
                          width: double.infinity,
                          child: FittedBox(
                            fit: BoxFit.fitWidth,
                            child: Image.asset('assets/donation_default.jpg'),
                          ),
                        ),
                        Expanded(
                          child: _detailContent(
                              title,
                              currentDonation,
                              targetDonation,
                              donationPercentage,
                              remainingDays,
                              countDonor,
                              receiver,
                              desc,
                              context,
                              donorsData,
                              isActive),
                        ),
                        _donationButton()
                      ],
                    ),
                  );
                } else {
                  return Scaffold(
                      appBar: AppBar(
                        title: const Text('Detail Donasi',
                            style: TextStyle(
                                fontSize: 20, fontFamily: 'Open Sans')),
                        backgroundColor: MyColor.color5(100),
                      ),
                      body: const Center(
                        child: Text("Donation Detail Not Found"),
                      ));
                }
              }
          }
        });
  }

  Widget _detailContent(
      String title,
      int currentDonation,
      int targetDonation,
      double donationPercent,
      String remainingDays,
      int countDonor,
      String receiver,
      String desc,
      BuildContext context,
      List donors,
      bool isActive) {
    return SingleChildScrollView(
        child: Column(
      children: [
        _generalDonationInfo(title, currentDonation, targetDonation),
        LinearPercentIndicator(
          lineHeight: 10,
          percent: donationPercent,
          progressColor: MyColor.color4(100),
          backgroundColor: MyColor.grey(80),
        ),
        _donationStat(countDonor, remainingDays, donationPercent),
        Divider(thickness: 1, color: MyColor.color1(80)),
        _fundraiserInfo(receiver),
        Divider(thickness: 1, color: MyColor.color1(80)),
        _additionalInfo(desc, context, donors, isActive),
      ],
    ));
  }

  Future<bool?> _attemptCloseFundraising(Tuple2<String, String> data) async {
    var donationID = widget.donationID;
    var jwt = await SecureStorage.readSecureData("jwt");
    var url = Uri.parse('$serverIP/api/v1/fundraisings/close/${donationID}');
    // await Future.delayed(Duration(seconds: 3));
    // var test = true;
    // log(test.toString());
    var res = await http.post(url,
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $jwt'
        },
        body: jsonEncode({
          "category": data.item1,
          "description": data.item2
        }));
    // Berhasil Menutup
    if (res.statusCode == 200 /*test*/) {
      setState(() {
        isActive = false;
      });
      return true;
      //  Gagal Menutup
    } else if (res.statusCode == 400 /*!test*/) {
      return false;
    } else {
      return null;
    }
  }

  Widget _shareButton() {
    return Container(
      margin: const EdgeInsets.only(top: 7, bottom: 10),
      height: 40,
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            spreadRadius: 0,
            blurRadius: 4,
            offset: const Offset(0, 2),
          ),
        ],
      ),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(primary: MyColor.color5(100)),
          onPressed: () {},
          child: Text(
            'Bagikan',
            style: TextStyle(
                color: MyColor.white(100),
                fontSize: 16,
                fontWeight: FontWeight.w800),
          )),
    );
  }

  Future<void> _closeFundraisingDialog() async {
    Widget _fieldLabel(String name) {
      return Container(
        width: 276,
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Text(
          name,
          style: const TextStyle(
              color: Colors.black, fontWeight: FontWeight.bold, fontSize: 14),
        ),
      );
    }

    Widget _dropdownmenu() {
      return Container(
        decoration: BoxDecoration(
            border: Border.all(color: Colors.blue),
            borderRadius: BorderRadius.circular(5)),
        width: 276,
        height: 33,
        padding: const EdgeInsets.all(5),
        child: DropdownButtonFormField<String>(
            isExpanded: true,
            iconEnabledColor: Colors.blue,
            // hint: Text('Pilih Disini', style: TextStyle(color: Color.fromRGBO(0, 0, 0, 0.5), fontSize: 10),),
            decoration: const InputDecoration.collapsed(
                hintText: 'Pilih Disini',
                hintStyle: TextStyle(
                    color: Color.fromRGBO(0, 0, 0, 0.5), fontSize: 10)),
            value: selectedCategory,
            onChanged: (String? kategori) {
              setState(() {
                selectedCategory = kategori;
              });
            },
            onSaved: (String? kategori) {
              setState(() {
                selectedCategory = kategori;
              });
            },
            items: list_kategori
                .map((String kategori) => DropdownMenuItem<String>(
                      value: kategori,
                      child: Text(
                        kategori,
                        style:
                            const TextStyle(color: Colors.black, fontSize: 10),
                      ),
                    ))
                .toList()),
      );
    }

    Widget _deskripsiTambahan() {
      return SizedBox(
        height: 96,
        child: TextField(
          decoration: const InputDecoration(
              contentPadding: EdgeInsets.all(5),
              hintText: "Ketik Disini",
              hintStyle:
                  TextStyle(color: Color.fromRGBO(0, 0, 0, 0.5), fontSize: 10),
              border: InputBorder.none,
              fillColor: Color.fromRGBO(0, 57, 169, 0.1),
              filled: true),
          maxLines: 5,
          controller: _closeDescription,
          style: const TextStyle(color: Colors.black, fontSize: 10),
        ),
      );
    }

    Widget _submitButton(BuildContext context) {
      return Container(
        height: 29,
        width: 276,
        decoration: const BoxDecoration(boxShadow: [
          BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.5),
              spreadRadius: 0,
              blurRadius: 4,
              offset: Offset(0, 2))
        ]),
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all(const Color.fromRGBO(222, 65, 67, 1)),
            textStyle: MaterialStateProperty.all(const TextStyle(fontSize: 14)),
            shadowColor:
                MaterialStateProperty.all(const Color.fromRGBO(0, 0, 0, 0.5)),
          ),
          child: Text(isLoading),
          onPressed: () async {
            var status = await _attemptCloseFundraising(Tuple2<String, String>(
                selectedCategory, _closeDescription.value.text));
            if (status == true) {
              Navigator.pop(context);
            } else {
              showDialog(
                  barrierDismissible: true,
                  context: context,
                  builder: (BuildContext context) => const AlertDialog(
                        title: Text("Error!"),
                        content: Text("Gagal Menutup Penggalangan Dana."),
                      ));
            }
          },
        ),
      );
    }

    return showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          return SizedBox(
            child: Material(
                color: const Color.fromRGBO(0, 0, 0, 0.1),
                child: Container(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: 276,
                    height: 242,
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5)),
                      child: Column(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.topRight,
                            height: 10,
                            width: 276,
                            child: IconButton(
                              color: const Color.fromRGBO(2, 125, 255, 1),
                              icon: const Icon(Icons.close),
                              iconSize: 20,
                              onPressed: () {
                                Navigator.pop(dialogContext);
                              },
                            ),
                          ),
                          _fieldLabel('Kategori'),
                          _dropdownmenu(),
                          _fieldLabel('Deskripsi Tambahan'),
                          _deskripsiTambahan(),
                          _submitButton(dialogContext)
                        ],
                      ),
                    ),
                  ),
                )),
          );
        });
  }

  Widget _closeDonationButton(bool isActive) {
    return FutureBuilder<dynamic>(
      future: SecureStorage.readSecureData("isUser"),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData && snapshot.data.toString() == "false") {
          if (isActive) {
            return Container(
              margin: const EdgeInsets.only(bottom: 20),
              height: 40,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.5),
                    spreadRadius: 0,
                    blurRadius: 4,
                    offset: const Offset(0, 2),
                  ),
                ],
              ),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: const Color.fromRGBO(222, 65, 65, 1)),
                  onPressed: () => _closeFundraisingDialog(),
                  child: const Text(
                    'Tutup',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w800),
                  )),
            );
          } else {
            return Container(
              margin: const EdgeInsets.only(bottom: 20),
              height: 40,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
              ),
              child: Center(
                child: Text('Closed', style: TextStyle(color: Colors.red,fontSize: 16,fontWeight: FontWeight.bold),),
              ),
            );
          }
        } else {
          return const SizedBox.shrink(); // Empty Widget
        }
      },
    );
  }

  Widget _donationButton() {
    var id = widget.donationID;
    return Container(
      height: 50,
      width: double.infinity,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            spreadRadius: 0,
            blurRadius: 4,
            offset: const Offset(0, 2),
          ),
        ],
      ),
      child: ElevatedButton(
          style: ButtonStyle(
              backgroundColor:
                  MaterialStateProperty.all<Color>(MyColor.color1(100)),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10))))),
          onPressed: () => Navigator.pushNamed(context, giveDonationRoute,
              arguments: {'id': id}),
          child: Text(
            'Donasi Sekarang',
            style: TextStyle(
                color: MyColor.white(100),
                fontSize: 18,
                fontWeight: FontWeight.w800),
          )),
    );
  }

  Widget _generalDonationInfo(
      String title, int currentDonation, int targetDonation) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 7, horizontal: 10),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Container(
          margin: const EdgeInsets.only(bottom: 7),
          child: Text(title,
              style:
                  const TextStyle(fontSize: 20, fontWeight: FontWeight.w700)),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                const Text("Terkumpul",
                    style:
                        TextStyle(fontSize: 12, fontWeight: FontWeight.w300)),
                Text('Rp${currencyFormatter.format(currentDonation)}',
                    style: const TextStyle(
                        fontSize: 16, fontWeight: FontWeight.w700)),
              ],
            ),
            Column(
              children: [
                const Text("Target",
                    style:
                        TextStyle(fontSize: 12, fontWeight: FontWeight.w300)),
                Text('Rp${currencyFormatter.format(targetDonation)}',
                    style: const TextStyle(
                        fontSize: 16, fontWeight: FontWeight.w700)),
              ],
            ),
          ],
        ),
      ]),
    );
  }

  Widget _donationStat(
      int countDonor, String remainingDays, double donationPercent) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 7, horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              Text(countDonor.toString(),
                  style: TextStyle(
                      color: MyColor.color6(100),
                      fontSize: 18,
                      fontWeight: FontWeight.w800)),
              const Text("Donatur",
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300)),
            ],
          ),
          Column(
            children: [
              Text(remainingDays,
                  style: TextStyle(
                      color: MyColor.color6(100),
                      fontSize: 18,
                      fontWeight: FontWeight.w800)),
              const Text("Hari Tersisa",
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300)),
            ],
          ),
          Column(
            children: [
              Text((donationPercent * 100).toStringAsFixed(2) + '%',
                  style: TextStyle(
                      color: MyColor.color6(100),
                      fontSize: 18,
                      fontWeight: FontWeight.w800)),
              const Text(
                "Terselesaikan",
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _fundraiserInfo(String receiver) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          CircleAvatar(
            radius: 20,
            backgroundImage: Image.asset("assets/profile_default.jpg").image,
          ),
          Container(
            margin: const EdgeInsets.only(left: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  "Penggalang Dana",
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
                ),
                Text(
                  receiver,
                  style: const TextStyle(
                      fontSize: 14, fontWeight: FontWeight.w700),
                )
              ],
            ),
          ),
          const Spacer(),
          SvgPicture.asset(
            'assets/verified_icon.svg',
          )
        ],
      ),
    );
  }

  Widget _additionalInfo(String desc, BuildContext context, List donors, bool isActive) {
    Widget profilpic = Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: const Color(0xFF3399FF)),
    );
    var donorsLength = donors.length;
    var donorsList = <Widget>[];
    for (var i = 0; i < donorsLength; i++) {
      donorsList.add(
        _donationItem(
            donors[i]['user']['name'],
            DateTime.parse(donors[i]['created_at'])
                .difference(DateTime.now())
                .inDays
                .toInt(),
            donors[i]['amount'],
            profilpic),
      );
    }
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 7),
          child: const Text(
            "Cerita",
            style: TextStyle(fontWeight: FontWeight.w800, fontSize: 16),
          ),
        ),
        ExpandText(
          desc,
          textAlign: TextAlign.justify,
          style: const TextStyle(
              fontSize: regularSize,
              fontWeight: FontWeight.w300,
              color: Colors.black),
          arrowColor: MyColor.color2(100),
          maxLines: 4,
        ),
        GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => Navigator.pushNamed(context, donaturRoute,
                arguments: {'id': widget.donationID}),
            child: Container(
              margin: const EdgeInsets.symmetric(vertical: 7),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    "Donatur Terkini",
                    style: TextStyle(fontWeight: FontWeight.w800, fontSize: 16),
                  ),
                  SvgPicture.asset(
                    'assets/arrow_left.svg',
                    width: 15,
                    height: 15,
                  )
                ],
              ),
            )),
        Column(
          children: donorsLength == 0
              ? [
                  const Text(
                    "Belum Ada Yang Berdonasi",
                    style: TextStyle(
                        fontSize: regularSize,
                        fontWeight: FontWeight.w300,
                        color: Colors.black),
                  ),
                ]
              : donorsList,
        ),
        _shareButton(),
        _closeDonationButton(isActive),
      ]),
    );
  }

  Future<dynamic> get _getDonationData async {
    var donationID = widget.donationID;
    var url = Uri.parse('$serverIP/api/v1/fundraisings/$donationID');
    var res = await http.get(url);
    if (res.statusCode == 200) return jsonDecode(res.body)["data"];
    return null;
  }

  Future<dynamic> get _getDonorsData async {
    var donationID = widget.donationID;
    var url = Uri.parse(
        '$serverIP/api/v1/fundraisings/${widget.donationID}/donations/?page=1&limit=2&order=desc');
    var res = await http.get(
      url,
      headers: {
        'accept': 'application/json',
        'Content-Type': 'application/json',
      },
    );
    if (res.statusCode == 200) return jsonDecode(res.body)["data"];
    return null;
  }

  Widget _donationItem(String name, int time, int amount, Widget profilpic) {
    return Container(
        decoration: BoxDecoration(
          color: const Color.fromRGBO(0, 57, 169, 0.1),
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
                color: Color.fromRGBO(0, 57, 169, 0.1),
                spreadRadius: 3,
                blurRadius: 7,
                offset: Offset(0.0, 0.75)),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: SizedBox(
            height: 70,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                AspectRatio(
                  aspectRatio: 1,
                  child: profilpic,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 0.0, 2.0, 0.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        const SizedBox(height: 15),
                        Row(children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  margin: const EdgeInsets.only(left: 5.0),
                                  child: Text(
                                    name,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14,
                                        fontFamily: 'Open Sans',
                                        color: Colors.black),
                                  ),
                                ),
                                const Padding(
                                    padding: EdgeInsets.only(bottom: 10.0)),
                                Container(
                                  margin: const EdgeInsets.only(left: 5.0),
                                  child: Text("berdonasi $time hari lalu",
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        fontFamily: 'Open Sans',
                                        fontWeight: FontWeight.normal,
                                        fontSize: 10.0,
                                        color: Colors.black,
                                      )),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: Column(children: <Widget>[
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: const <Widget>[],
                              ),
                              const Padding(
                                  padding: EdgeInsets.only(bottom: 5.0)),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Expanded(
                                    child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            5, 0, 0, 0),
                                        child: Text(
                                          "+ Rp $amount",
                                          style: const TextStyle(
                                            fontFamily: 'Open Sans',
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18.0,
                                            color: Color(0xDD1392D3),
                                          ),
                                        )),
                                  ),
                                ],
                              )
                            ]),
                          ),
                        ]),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
