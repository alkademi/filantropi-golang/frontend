import 'package:flutter/material.dart';
import 'package:tes/app.dart';
import 'package:tes/style.dart';
import 'login_form.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          const Spacer(
            flex: 1,
          ),
          _filantropiLogo(),
          const LoginForm(),
          const Spacer(
            flex: 3,
          ),
          _toRegister(context),
          const Padding(padding: EdgeInsets.only(bottom: 15))
        ],
      ),
    );
  }

  Widget _filantropiLogo() {
    return Padding(
      padding: const EdgeInsets.only(top: 60.0),
      child: Text(
        'filantropi.id',
        style: TextStyle(
          fontFamily: fontLogo,
          fontSize: largeSize,
          color: MyColor.color1(100),
        ),
      ),
    );
  }

  Widget _toRegister(BuildContext context) {
    return Column(children: <Widget>[
      Divider(thickness: 1, color: MyColor.color5(100)),
      Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
        Text(
          'Tidak Punya Akun? ',
          style: TextStyle(color: MyColor.color5(100)),
        ),
        GestureDetector(
            onTap: () => Navigator.pushNamed(context, registerRoute,
                arguments: <String, dynamic>{}),
            child: Text(
              'Buat Akun',
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: MyColor.color5(100)),
            ))
      ])
    ]);
  }
}
