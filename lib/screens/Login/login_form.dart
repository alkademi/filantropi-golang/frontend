import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:tes/app.dart';
import '../../style.dart';
import 'package:http/http.dart' as http;
import '../../utils/secure_storage.dart';
import '../../constants/server_ip.dart';
import 'package:tuple/tuple.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  _LoginForm createState() => _LoginForm();
}

class _LoginForm extends State<LoginForm> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _invalid = false;

  @override
  Widget build(BuildContext context) {
    return Builder(
        builder: (context) => Form(
            key: _formKey,
            child: Container(
              height: _invalid ? 170 : 200,
              margin: const EdgeInsets.only(left: 15, right: 15),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    _usernameInput(),
                    _passwordInput(),
                    _submitButton(),
                  ]),
            )));
  }

  Widget _usernameInput() {
    return TextFormField(
      controller: _usernameController,
      validator: (value) => _usernameValidator(value),
      cursorColor: MyColor.white(100),
      style: TextStyle(color: MyColor.white(100), fontSize: regularSize),
      decoration: InputDecoration(
          filled: true,
          fillColor: MyColor.color1(20),
          errorStyle: const TextStyle(fontSize: regularSize),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(width: 0, color: MyColor.color1(20))),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(width: 0, color: MyColor.color1(20))),
          contentPadding: const EdgeInsets.only(left: 10, top: 11, bottom: 10),
          hintText: 'Username',
          hintStyle:
              TextStyle(color: MyColor.white(100), fontSize: regularSize)),
    );
  }

  Widget _passwordInput() {
    return TextFormField(
      controller: _passwordController,
      obscureText: true,
      validator: (value) => _passwordValidator(value),
      cursorColor: MyColor.white(100),
      style: TextStyle(color: MyColor.white(100), fontSize: regularSize),
      decoration: InputDecoration(
          filled: true,
          fillColor: MyColor.color1(20),
          errorStyle: const TextStyle(fontSize: regularSize),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(width: 0, color: MyColor.color1(20)),
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(width: 0, color: MyColor.color1(20))),
          contentPadding: const EdgeInsets.only(left: 10, top: 11, bottom: 10),
          hintText: 'Kata Sandi',
          hintStyle:
              TextStyle(color: MyColor.white(100), fontSize: regularSize)),
    );
  }

  Widget _submitButton() {
    return Container(
      height: 40,
      width: double.infinity,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(primary: MyColor.color5(100)),
          onPressed: () => _onSubmit(),
          child: Text(
            'Masuk',
            style: TextStyle(color: MyColor.white(100), fontSize: regularSize),
          )),
    );
  }

  String? _usernameValidator(String? value) {
    if (value!.isEmpty) {
      _invalid = true;
      return 'Input Ini Harus Diisi';
    }
    // String regex =
    //     r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    // if (!RegExp(regex).hasMatch(value)) {
    //   _invalid = true;
    //   return 'Format username salah';
    // }
    _invalid = false;
    return null;
  }

  String? _passwordValidator(String? value) {
    if (value!.isEmpty) {
      _invalid = true;
      return 'Input Ini Harus Diisi';
    }
    _invalid = false;
    return null;
  }

  Future<void> _onSubmit() async {
    if (_formKey.currentState!.validate()) {
      var username = _usernameController.text;
      var password = _passwordController.text;
      var data = await attemptLogIn(username, password);
      var jwt = data?.item1;
      var isUser = data?.item2;
      if (jwt != null && isUser != null) {
        SecureStorage.writeSecureData("jwt", jwt);
        SecureStorage.writeSecureData("isUser", isUser.toString());
        Navigator.pushReplacementNamed(context, homeRoute,
            arguments: {'jwt': jwt});
      } else {
        displayDialog(context, "Terjadi Error",
            "Tidak ada akun dengan username dan password yang sesuai");
      }
    }
  }

  Future<Tuple2<String, bool>?> attemptLogIn(
      String username, String password) async {
    var url = Uri.parse('$serverIP/api/v1/auth/login');
    var res = await http.post(url,
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: jsonEncode({"Username": username, "Password": password}));
    if (res.statusCode == 202) {
      return Tuple2<String, bool>(jsonDecode(res.body)["data"]["token"],
          jsonDecode(res.body)["data"]["isUser"]);
    }
    return null;
  }

  void displayDialog(context, title, text) => showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(title: Text(title), content: Text(text)),
      );
}
