// ignore_for_file: file_names, use_key_in_widget_constructors, non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:tes/app.dart';
import '../../style.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:http/http.dart' as http;
import '../../constants/server_ip.dart';
import 'dart:convert';

class DaftarDonasi extends StatelessWidget {
  const DaftarDonasi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Fundraising(),
    );
  }
}

class Fundraising extends StatefulWidget {
  @override
  _FundraisingState createState() => _FundraisingState();
}

class _FundraisingState extends State<Fundraising> {
  getFundraisingData() async {
    var url = Uri.parse('$serverIP/api/v1/fundraisings');
    var res = await http.get(url);
    var jsonData = jsonDecode(res.body)["data"];

    List<Fund> funds = [];

    for (var u in jsonData) {
      Fund fund = Fund(u["id"], u["title"], u["receiver"], u["target_date"],
          u["target_donation"], u["current_donation"]);
      funds.add(fund);
    }
    return funds;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Donasi',
              style: TextStyle(fontSize: 20, fontFamily: 'Open Sans')),
          backgroundColor: MyColor.color5(100),
        ),
        body: Card(
          child: FutureBuilder(
            future: getFundraisingData(),
            builder: (context, snapshot) {
              if (snapshot.data == null) {
                return const Center(
                  child: Text('Loading ...'),
                );
              } else {
                var data = snapshot.data as List<Fund>;
                return ListView.builder(
                  shrinkWrap: true,
                  itemCount: data.length,
                  itemBuilder: (context, i) {
                    var targetDate = DateTime.parse(data[i].target_date);
                    var id = data[i].id;
                    var donationPercentage = data[i].current_donation /
                        data[i].target_donation *
                        100;
                    var title = data[i].title;
                    var currentDonation = data[i].current_donation;
                    var receiver = data[i].receiver;
                    var remainingDays =
                        targetDate.difference(DateTime.now()).inDays.toInt();
                    return Row(
                      children: [
                        Expanded(
                            child: BaseListDonation(
                          id: id,
                          title: title,
                          remainDuration: remainingDays > 0 ? remainingDays : 0,
                          progress: donationPercentage <= 1.0
                              ? donationPercentage
                              : 1.0,
                          author: receiver,
                          collected: currentDonation,
                          thumbnail: Container(
                            decoration: BoxDecoration(
                              image: const DecorationImage(
                                image:
                                    AssetImage('assets/donation_default.jpg'),
                                fit: BoxFit.fill,
                              ),
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ))
                      ],
                    );
                  },
                );
              }
            },
          ),
        ));
  }
}

class Fund {
  final int id, target_donation, current_donation;
  final String title, receiver, target_date;

  Fund(this.id, this.title, this.receiver, this.target_date,
      this.target_donation, this.current_donation);
}

class _DonationDescription extends StatelessWidget {
  const _DonationDescription({
    Key? key,
    required this.title,
    required this.author,
    required this.progress,
    required this.remainDuration,
    required this.collected,
  }) : super(key: key);

  final String author;
  final int collected;
  final double progress;
  final int remainDuration;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 2,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(left: 5.0),
                child: Text(
                  title,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      fontFamily: 'Open Sans',
                      color: Colors.black),
                ),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 10.0)),
              LinearPercentIndicator(
                width: 220.0,
                lineHeight: 5.0,
                percent: progress,
                progressColor: Colors.blue,
              ),
              const Padding(padding: EdgeInsets.only(bottom: 10.0)),
              Container(
                margin: const EdgeInsets.only(left: 5.0),
                child: Text(author,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      fontFamily: 'Open Sans',
                      fontWeight: FontWeight.normal,
                      fontSize: 14.0,
                      color: Colors.black,
                    )),
              ),
            ],
          ),
        ),
        Expanded(
          flex: 1,
          child: Column(children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: const <Widget>[
                Expanded(
                  child: Padding(
                      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                      child: Text(
                        "Terkumpul",
                        style: TextStyle(
                          fontFamily: 'Open Sans',
                          fontWeight: FontWeight.normal,
                          fontSize: 12.0,
                          color: Colors.black87,
                        ),
                      )),
                ),
                Expanded(
                  child: Text(
                    "Sisa hari",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'Open Sans',
                      fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      color: Colors.black87,
                    ),
                  ),
                )
              ],
            ),
            const Padding(padding: EdgeInsets.only(bottom: 5.0)),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Expanded(
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
                      child: Text(
                        "Rp $collected",
                        style: const TextStyle(
                          fontFamily: 'Open Sans',
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,
                          color: Colors.black87,
                        ),
                      )),
                ),
                Expanded(
                  child: Text(
                    "$remainDuration",
                    textAlign: TextAlign.right,
                    style: const TextStyle(
                      fontFamily: 'Open Sans',
                      fontWeight: FontWeight.bold,
                      fontSize: 14.0,
                      color: Colors.black87,
                    ),
                  ),
                )
              ],
            )
          ]),
        ),
      ],
    );
  }
}

class BaseListDonation extends StatelessWidget {
  const BaseListDonation({
    Key? key,
    required this.thumbnail,
    required this.title,
    required this.author,
    required this.progress,
    required this.remainDuration,
    required this.collected,
    required this.id,
  }) : super(key: key);

  final String author;
  final int collected;
  final double progress;
  final int remainDuration;
  final Widget thumbnail;
  final String title;
  final int id;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () =>
            Navigator.pushNamed(context, donationDetail, arguments: {'id': id}),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: SizedBox(
            height: 130,
            child: Container(
                color: Colors.white,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    AspectRatio(
                      aspectRatio: 1.25,
                      child: thumbnail,
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10.0, 0.0, 2.0, 0.0),
                        child: _DonationDescription(
                          title: title,
                          author: author,
                          progress: progress,
                          remainDuration: remainDuration,
                          collected: collected,
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ));
  }
}
