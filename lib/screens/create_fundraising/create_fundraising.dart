import 'package:flutter/material.dart';
import 'package:tes/style.dart';
import 'fundraising_form.dart';

class CreateFundraising extends StatelessWidget {
  const CreateFundraising({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: <Widget>[
          _filantropiLogo(),
          const FundraisingForm(),
        ],
      ),
    ));
  }

  Widget _filantropiLogo() {
    return Padding(
      padding: const EdgeInsets.only(top: 60.0),
      child: Text(
        'Create Fundraising',
        style: TextStyle(
          fontFamily: fontLogo,
          fontSize: largeSize,
          color: MyColor.color1(100),
        ),
      ),
    );
  }
}
