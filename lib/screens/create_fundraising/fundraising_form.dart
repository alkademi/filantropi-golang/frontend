import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../../style.dart';
import 'package:http/http.dart' as http;
import '../../utils/secure_storage.dart';
import '../../constants/server_ip.dart';
import 'package:intl/intl.dart';

class FundraisingForm extends StatefulWidget {
  const FundraisingForm({Key? key}) : super(key: key);

  @override
  _FundraisingForm createState() => _FundraisingForm();
}

class _FundraisingForm extends State<FundraisingForm> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descController = TextEditingController();
  final TextEditingController _receiverController = TextEditingController();
  final TextEditingController _targetDateController = TextEditingController();
  final TextEditingController _targetDonationController =
      TextEditingController();
  final _formKey = GlobalKey<FormState>();
  DateTime _selectedDate = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                _inputForm("Judul", _titleController),
                _inputForm("Deskripsi", _descController),
                _inputForm("Penerima", _receiverController),
                _numberInputForm("Target Donasi", _targetDonationController),
                _targetDateInput(),
                _submitButton(),
              ]),
        ));
  }

  Widget _inputForm(String name, TextEditingController controller) {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
        child: TextFormField(
          controller: controller,
          validator: (value) => _validator(value),
          cursorColor: MyColor.white(100),
          style: TextStyle(color: MyColor.white(100), fontSize: regularSize),
          decoration: InputDecoration(
              filled: true,
              fillColor: MyColor.color1(20),
              errorStyle: const TextStyle(fontSize: regularSize),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(width: 0, color: MyColor.color1(20))),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(width: 0, color: MyColor.color1(20))),
              contentPadding:
                  const EdgeInsets.only(left: 10, top: 11, bottom: 10),
              hintText: name,
              hintStyle:
                  TextStyle(color: MyColor.white(100), fontSize: regularSize)),
        ));
  }

  Widget _targetDateInput() {
    return InkWell(
        onTap: () {
          _selectDate(context);
        },
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
          child: TextFormField(
            style: TextStyle(color: MyColor.white(100), fontSize: regularSize),
            textAlign: TextAlign.center,
            enabled: false,
            validator: (value) => _validator(value),
            keyboardType: TextInputType.text,
            controller: _targetDateController,
            decoration: InputDecoration(
                filled: true,
                fillColor: MyColor.color1(20),
                errorStyle:
                    const TextStyle(fontSize: regularSize, color: Colors.red),
                errorBorder: const UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.red)),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide:
                        BorderSide(width: 0, color: MyColor.color1(20))),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide:
                        BorderSide(width: 0, color: MyColor.color1(20))),
                contentPadding:
                    const EdgeInsets.only(left: 10, top: 11, bottom: 10),
                hintText: "Target Tanggal",
                hintStyle: TextStyle(
                    color: MyColor.white(100), fontSize: regularSize)),
          ),
        ));
  }

  Widget _numberInputForm(String name, TextEditingController controller) {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
        child: TextFormField(
          controller: controller,
          validator: (value) => _validator(value),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ],
          cursorColor: MyColor.white(100),
          style: TextStyle(color: MyColor.white(100), fontSize: regularSize),
          decoration: InputDecoration(
              filled: true,
              fillColor: MyColor.color1(20),
              errorStyle: const TextStyle(fontSize: regularSize),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(width: 0, color: MyColor.color1(20))),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(width: 0, color: MyColor.color1(20))),
              contentPadding:
                  const EdgeInsets.only(left: 10, top: 11, bottom: 10),
              hintText: name,
              hintStyle:
                  TextStyle(color: MyColor.white(100), fontSize: regularSize)),
        ));
  }

  Widget _submitButton() {
    return Container(
      height: 40,
      width: double.infinity,
      margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(primary: MyColor.color5(100)),
          onPressed: () => _onSubmit(),
          child: Text(
            'Buat',
            style: TextStyle(color: MyColor.white(100), fontSize: regularSize),
          )),
    );
  }

  String? _validator(String? value) {
    if (value!.isEmpty) {
      return 'Input Ini Harus Diisi';
    }
    return null;
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: _selectedDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime.now(),
        lastDate: DateTime(2101));
    if (picked != null) {
      setState(() {
        _selectedDate = picked;
        _targetDateController.text = DateFormat.yMd().format(_selectedDate);
      });
    }
  }

  Future<void> _onSubmit() async {
    if (_formKey.currentState!.validate()) {
      String title = _titleController.text;
      String desc = _descController.text;
      String receiver = _receiverController.text;
      int targetDonation = int.parse(_targetDonationController.text);
      String targetDate = _selectedDate.toUtc().toIso8601String();
      var res = await _createFundraising(
          title, desc, receiver, targetDonation, targetDate);
      if (res != null) {
        Navigator.pop(context);
      } else {
        displayDialog(
            context, "Terjadi Error", "Penggalangan Dana Gagal Dibuat");
      }
    }
  }

  Future<String?> _createFundraising(String title, String desc, String receiver,
      int targetDonation, String targetDate) async {
    var url = Uri.parse('$serverIP/api/v1/fundraisings/');
    var jwt = await SecureStorage.readSecureData("jwt");
    var res = await http.post(url,
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $jwt'
        },
        body: jsonEncode({
          "title": title,
          "description": desc,
          "receiver": receiver,
          "target_donation": targetDonation,
          "target_date": targetDate
        }));
    if (res.statusCode == 201) {
      return jsonDecode(res.body)["message"];
    } else {
      displayDialog(context, "Error", jsonDecode(res.body).toString());
    }
    return null;
  }

  void displayDialog(context, title, text) => showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(title: Text(title), content: Text(text)),
      );
}
