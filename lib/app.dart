import 'package:flutter/material.dart';
import 'package:tes/screens/create_fundraising/create_fundraising.dart';
import 'package:tes/screens/donation_detail/donation_detail.dart';
import 'package:tes/screens/beri_donasi/beri_donasi.dart';
import 'package:tes/screens/beri_donasi/pembayaran.dart';
import 'package:tes/screens/riwayat/riwayat.dart';
import 'package:tes/screens/search/search.dart';
import 'screens/Login/login.dart';
import 'screens/register/register.dart';
import 'screens/Homepage/homepage.dart';
import 'screens/daftar/daftar-donasi.dart';
import 'screens/daftar_donatur/donatur.dart';
import 'utils/secure_storage.dart';
import 'dart:convert' show json, base64, ascii;

const loginRoute = "/";
const registerRoute = '/register';
const homeRoute = '/home';
const createFundrasing = '/fundraising/create';
const donationRoute = '/fundraising';
const donaturRoute = 'fundraising/donatur';
const donationDetail = '/fundrasings_details';
const donationHistory = '/fundrasings/history';
const searchRoute = '/search';
const payRoute = 'fundraising/donate/pay';
const giveDonationRoute = 'fundraising/donate';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: _routes(),
      theme: ThemeData(fontFamily: 'OpenSans'),
      home: FutureBuilder(
          future: jwtOrEmpty,
          builder: (context, snapshot) {
            if (!snapshot.hasData) return const CircularProgressIndicator();
            if (snapshot.data != "") {
              var data = snapshot.data;
              String str = data.toString();
              var jwt = str.split(".");

              if (jwt.length != 3) {
                return const Login();
              } else {
                var payload = json.decode(
                    ascii.decode(base64.decode(base64.normalize(jwt[1]))));
                if (DateTime.fromMillisecondsSinceEpoch(payload["exp"] * 1000)
                    .isAfter(DateTime.now())) {
                  return HomePage(str, payload);
                } else {
                  return const Login();
                }
              }
            } else {
              return const Login();
            }
          }),
    );
  }

  RouteFactory _routes() {
    return (settings) {
      final Map<String, dynamic> arguments =
          settings.arguments as Map<String, dynamic>;
      Widget screen;
      switch (settings.name) {
        case loginRoute:
          screen = const Login();
          break;
        case registerRoute:
          screen = const Register();
          break;
        case homeRoute:
          screen = HomePage.fromBase64(arguments['jwt']);
          break;
        case createFundrasing:
          screen = const CreateFundraising();
          break;
        case donationRoute:
          screen = const DaftarDonasi();
          break;
        case donaturRoute:
          screen = DaftarDonatur(arguments['id']);
          break;
        case donationDetail:
          screen = DonationDetail(arguments['id']);
          break;
        case donationHistory:
          screen = const Riwayat();
          break;
        case searchRoute:
          screen = const Search();
          break;
        case giveDonationRoute:
          screen = BeriDonasi(arguments['id']);
          break;
        case payRoute:
          screen = PayDonation(arguments['url']);
          break;
        default:
          screen = this;
      }
      return MaterialPageRoute(builder: (BuildContext context) => screen);
    };
  }

  Future<String> get jwtOrEmpty async {
    var jwt = await SecureStorage.readSecureData("jwt");
    if (jwt == null) return "";
    return jwt;
  }
}
